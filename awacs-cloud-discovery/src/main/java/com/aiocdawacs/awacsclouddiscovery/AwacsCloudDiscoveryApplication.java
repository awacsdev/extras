package com.aiocdawacs.awacsclouddiscovery;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Bean;

import com.netflix.appinfo.ApplicationInfoManager;
import com.netflix.appinfo.EurekaInstanceConfig;

@EnableEurekaServer
@SpringBootApplication
public class AwacsCloudDiscoveryApplication {

	public static void main(String[] args) {
        new SpringApplicationBuilder(AwacsCloudDiscoveryApplication.class).run(args);
    }
	

    @Bean
    public ApplicationInfoManager getApplicationInfoManager(EurekaInstanceConfig config) {
    	  @SuppressWarnings("deprecation")
		ApplicationInfoManager applicationInfoManager = new ApplicationInfoManager(config);
    	  return applicationInfoManager;
    }
}
