package com.aiocdawacs;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;

import com.github.vanroy.cloud.dashboard.config.EnableCloudDashboard;
import com.github.vanroy.cloud.dashboard.repository.ApplicationRepository;
import com.github.vanroy.cloud.dashboard.repository.eureka.RemoteEurekaRepository;
import com.netflix.discovery.EurekaClient;

@EnableCloudDashboard
@SpringBootApplication
public class ServicesDashboardApplication {

	public static void main(String[] args) {
        new SpringApplicationBuilder(ServicesDashboardApplication.class).run(args);
    }
	
	@Bean
	public ApplicationRepository getApplicationRepository(EurekaClient eurekaClient) {
		return new RemoteEurekaRepository(eurekaClient);
	}
}
